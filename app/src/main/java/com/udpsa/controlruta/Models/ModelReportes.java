package com.udpsa.controlruta.Models;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by LenovoPc_2 on 20/09/2016.
 */
public class ModelReportes {



    public String numeroScanneos(SQLiteDatabase db) {
        String numeroScanneos = "";
        Cursor n = db.rawQuery("SELECT COUNT(*) FROM Reportes", null);
        n.moveToFirst();
        numeroScanneos = n.getString(0);
        n.close();

        return numeroScanneos;
    }

    public void cambioEstadoReporte(SQLiteDatabase db){
        db.execSQL("UPDATE Reportes SET estadoEnvio='1' ");
    }
    public void insertarReporte(SQLiteDatabase db, String fecha,String hora, String comentario, Bitmap imagen){



        ByteArrayOutputStream bos= new ByteArrayOutputStream();
        imagen.compress(Bitmap.CompressFormat.PNG,100,bos);
        byte [] array=bos.toByteArray();

        System.out.println("imagen "+array);


        String base64Image = Base64.encodeToString(array, Base64.DEFAULT);

        System.out.println("base 64 "+base64Image);




        db.execSQL("INSERT INTO Reportes (fecha,hora,comentario,imagen,estadoEnvio) VALUES ('"+fecha+"','"+hora+"','"+comentario+"','"+base64Image+"','0')");



    }

    public String getReporte(SQLiteDatabase db,ArrayList<String> listUsario){

        JSONArray jsonArray = new JSONArray();

        JSONObject jsonPrincipal = new JSONObject();
        Cursor c=db.rawQuery("SELECT * FROM Reportes WHERE estadoEnvio='0'",null);
        while (c.moveToNext()){


            System.out.println("fecha "+c.getString(1));
            System.out.println("hora "+c.getString(2));
            System.out.println("comentario "+c.getString(3));
            System.out.println("imagen "+c.getString(4));
            System.out.println("estadoEnvio "+c.getString(5));

            JSONObject jsonReportes = new JSONObject();

            try {
                jsonReportes.put("usuario", listUsario.get(0));
                jsonReportes.put("user_id", listUsario.get(1));
                jsonReportes.put("fecha",c.getString(1));
                jsonReportes.put("hora",c.getString(2));
                jsonReportes.put("comentario",c.getString(3));
                jsonReportes.put("imagen",c.getString(4));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            jsonArray.put(jsonReportes);

        }

        try {
            jsonPrincipal.put("reportes",jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        c.close();


        return jsonPrincipal.toString();
    }
}
