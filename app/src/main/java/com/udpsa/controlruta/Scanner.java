package com.udpsa.controlruta;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.udpsa.controlruta.Util.IntentIntegrator;
import com.udpsa.controlruta.Util.IntentResult;
import com.udpsa.controlruta.Util.Useful;

public class Scanner extends AppCompatActivity {

    private Button btnScannear;
    private TextView txtResultado;
    String scanneo;

    Useful use;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        use= new Useful();
        btnScannear = (Button) findViewById(R.id.btnScannear);
        txtResultado = (TextView) findViewById(R.id.txtResultado);

        btnScannear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new IntentIntegrator(Scanner.this).initiateScan();
            }
        });
    }


    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_scanneo, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.volver:
                Salir();
                return true;
        }
        return true;
    }

    public void Salir() {
        AlertDialog.Builder alert = new AlertDialog.Builder(Scanner.this);
        alert.setMessage("¿Esta seguro que desea regresar?");
        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent();
                i.setClass(Scanner.this, MenuScanneos.class);
                startActivity(i);
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });

        alert.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //Se obtiene el resultado del proceso de scaneo y se parsea
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            //Quiere decir que se obtuvo resultado pro lo tanto:
            //Desplegamos en pantalla el contenido del código de barra scaneado


            String scanContent = scanningResult.getContents();
            System.out.println("resultado " + scanContent);
            scanneo=scanContent;
            txtResultado.setText(scanContent);

        } else {
            //Quiere decir que NO se obtuvo resultado
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No se ha recibido datos del scaneo!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void enviarScanneo(View view) {


        Intent i = new Intent();
        System.out.println("scanneo "+scanneo);

        if(scanneo.equals("") || scanneo.equals("...")){
            AlertDialog.Builder alert= new AlertDialog.Builder(Scanner.this);
            alert.setMessage("Debe de realizar un scanneo para continuar");
            alert.setPositiveButton("Aceptar ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
               dialog.dismiss();

                }
            });

            alert.show();
            return;
        }


       /* i.setClass(Scanner.this, MapsActivity.class);
        i.putExtra("scanneo",scanneo);
        i.putExtra("fecha",use.getFecha());
        i.putExtra("hora",use.getHora());
        startActivity(i);
        android.os.Process.killProcess(android.os.Process.myPid());*/



        Intent intent = new Intent(this, ServiceLocation.class);
        intent.putExtra("scanneo",scanneo);
        intent.putExtra("fecha",use.getFecha());
        intent.putExtra("hora",use.getHora());
        startService(intent);
    }
}
