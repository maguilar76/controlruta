package com.udpsa.controlruta.Models;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by LenovoPc_2 on 16/09/2016.
 */
public class ModelUsuarios {

    public  ArrayList<String> getUser(SQLiteDatabase db){
        ArrayList<String> listUsario= new ArrayList<String>();

        System.out.println("get user");
        String usuario="";
        String userId="";

        Cursor u=db.rawQuery("SELECT nombre,userId FROM usuarios limit 1",null);
        u.moveToFirst();
        usuario=u.getString(0);
        userId=u.getString(1);
        u.close();

        listUsario.add(usuario);
        listUsario.add(userId);

        System.out.println("usuario "+usuario);
        return listUsario;
    }

    public void insertUsuarios(String user,String password,String nombre,String apellido,String user_id,SQLiteDatabase db){

        System.out.println("INSERT INTO usuarios(user,password,nombre,apellido,userId) VALUES ('" + user + "','" + password + "','" + nombre + "','"+apellido+"','"+user_id+"')");

        db.execSQL("INSERT INTO usuarios(user,password,nombre,apellido,userId) VALUES ('" + user + "','" + password + "','" + nombre + "','"+apellido+"','"+user_id+"')");
    }

    public ArrayList<String> getNombreUser(SQLiteDatabase db){
        ArrayList<String> listUsario= new ArrayList<String>();
        String usuario="";
        String user="";
        Cursor c = db.rawQuery("SELECT nombre,user FROM usuarios", null);
        c.moveToFirst();
        usuario = c.getString(0);
        user=c.getString(1);
        c.close();

        listUsario.add(usuario);
        listUsario.add(user);

        return listUsario;

    }


}
