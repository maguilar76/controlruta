package com.udpsa.controlruta;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.*;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.udpsa.controlruta.Connection.DataBase;
import com.udpsa.controlruta.Connection.DownloadInformation;
import com.udpsa.controlruta.Models.ModelUsuarios;
import com.udpsa.controlruta.Util.Useful;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.support.v4.app.ActivityCompat.startActivity;

public class MainActivity extends AppCompatActivity {
    private TextView txtUser;
    private TextView txtPassword;

    Useful use;

    String usuario;
    String contrasena;

    String estado;
    String nombre;
    String cliente;

    String user_id;

    String resp;

    DownloadInformation download;

    DataBase Database = new DataBase(this);
    SQLiteDatabase db;
    ModelUsuarios modelUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        modelUsuarios= new ModelUsuarios();

        use = new Useful();
        db = Database.getWritableDatabase();

        txtUser = (TextView) findViewById(R.id.txtUser);
        txtPassword = (TextView) findViewById(R.id.txtPassword);

        download = new DownloadInformation();
    }


    public void ingresar(View view) {
        usuario = txtUser.getText().toString().trim();
        contrasena = txtPassword.getText().toString().trim();

        System.out.println("usuario " + usuario);
        System.out.println("contrasena " + contrasena);

        AlertDialog.Builder ad = new AlertDialog.Builder(MainActivity.this);


        if (usuario.equals("")) {
            ad.setMessage("Debe ingresar un usuario");
            ad.setPositiveButton("Aceptar ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            ad.show();
            return;
        }


        if (contrasena.equals("")) {
            ad.setMessage("Debe ingresar una contraseña");
            ad.setPositiveButton("Aceptar ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            ad.show();
            return;
        }


        if (use.hasInternet(MainActivity.this)) {
            AsyncLogin asyncLogin = new AsyncLogin();
            asyncLogin.execute();
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
            alert.setCancelable(false);
            alert.setMessage("Señor usuario,No tiene conexión a internet");
            alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
            return;
        }


    }

    public void salir(View view) {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setMessage("¿Esta seguro que desea salir?");
        alert.setPositiveButton("Aceptar ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });

        alert.show();
    }


    class AsyncLogin extends AsyncTask<Void, Void, Void> {

        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        String imei=use.getIMEI(MainActivity.this);



        @Override
        protected void onPreExecute() {

            this.dialog.setCancelable(false);
            this.dialog.setMessage("Validando Datos\nPor favor Espere");
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            resp = download.validateInformation(usuario, contrasena,imei);

            System.out.println("respuesta " + resp);
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            try {
                JSONObject jsonObject = new JSONObject(resp);
                user_id = jsonObject.get("cliente").toString();
                nombre = jsonObject.get("nombre").toString();
                estado = jsonObject.get("estado").toString();


                System.out.println("estado " + jsonObject.get("estado"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!estado.equals("0")) {
               if(estado.equals("1")){
                   modelUsuarios.insertUsuarios(usuario,contrasena,nombre,"Apellido",user_id,db);
                   Intent i = new Intent();
                   i.setClass(MainActivity.this, MenuScanneos.class);
                   startActivity(i);
                   android.os.Process.killProcess(android.os.Process.myPid());
               }else if(estado.equals("2")){
                   this.dialog.dismiss();
                   final AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                   alert.setTitle("Alerta");
                   alert.setMessage("Señor usuario el imei de este teléfono no se encuentra registrado, por favor comuniquese con el administrador del sistema");
                   alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialogInterface, int i) {
                           dialogInterface.dismiss();
                       }
                   });
                   alert.show();

                   return;
               }

            } else {
                this.dialog.dismiss();
                final AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Alerta");
                alert.setMessage("Usuario o clave incorrecto");
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alert.show();

                return;
            }

            this.dialog.dismiss();
        }
    }
}
