package com.udpsa.controlruta.Connection;

import android.database.sqlite.SQLiteDatabase;

import com.udpsa.controlruta.Models.ModelReportes;
import com.udpsa.controlruta.Models.ModelScanneos;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LenovoPc_2 on 15/09/2016.
 */
public class SendInformation {
    ModelReportes modelReportes = new ModelReportes();
    ModelScanneos modelScanneos = new ModelScanneos();


    public void enviarInformacion(SQLiteDatabase db, ArrayList<String> listUsario) {
        String response = "";

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost("https://informes.udpsa.com/svirtual/ControlRuta_Registro.php");

        List<NameValuePair> parameters = new ArrayList<NameValuePair>();

        //System.out.println("jsonPrincipal " + jsonPrincipal.toString());

        String reportes = modelReportes.getReporte(db, listUsario);
        String scanneos = modelScanneos.getScanneos(db, listUsario);


        System.out.println("json reportes " + reportes);
        System.out.println("json scanneos " + scanneos);

        parameters.add(new BasicNameValuePair("scanneos", scanneos));
        parameters.add(new BasicNameValuePair("reportes", reportes));

        try {

            post.setEntity(new UrlEncodedFormEntity(parameters));


            HttpResponse resp = httpClient.execute(post);


            HttpEntity ent = resp.getEntity();


            response = EntityUtils.toString(ent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
