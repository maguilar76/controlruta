package com.udpsa.controlruta;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;


import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.udpsa.controlruta.Connection.DataBase;
import com.udpsa.controlruta.Connection.SendInformation;
import com.udpsa.controlruta.Models.ModelScanneos;
import com.udpsa.controlruta.Models.ModelUsuarios;
import com.udpsa.controlruta.Util.Useful;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ServiceLocation extends Service {
    DataBase Database = new DataBase(this);
    SQLiteDatabase db;
    String scanneo;
    String fecha;
    String hora;
    String usuario;

    Useful use;
    ModelScanneos modelScanneos;


    SendInformation send;
    ModelUsuarios modelUsuarios;

//    PowerManager.WakeLock wakeLock;

    private LocationManager locationManager;
    //DBController controller = new DBController(this);



    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }


    public int onStartCommand (Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);
        Bundle bundle = intent.getExtras();
        scanneo = bundle.getString("scanneo");
        fecha = bundle.getString("fecha");
        hora = bundle.getString("hora");





        return flags;
    }

    @Override
    public void onCreate() {


        use = new Useful();
        send = new SendInformation();
        modelScanneos = new ModelScanneos();
        modelUsuarios = new ModelUsuarios();

        db = Database.getWritableDatabase();


        // TODO Auto-generated method stub
        super.onCreate();

//        PowerManager pm = (PowerManager) getSystemService(this.POWER_SERVICE);

//        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotSleep");

        // Toast.makeText(getApplicationContext(), "Service Created",
        // Toast.LENGTH_SHORT).show();

        Log.e("Google", "Service Created");

    }

    @Override
    @Deprecated
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);

        Log.e("Google", "Service Started");

        locationManager = (LocationManager) getApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60 * 1000 * 2, 0, listener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60 * 1000 * 5, 0, listener);
//        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,0, 0, listener);

    }

    private LocationListener listener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            // TODO Auto-generated method stub

            Log.e("Google", "Location Changed");

            if (location == null)
                return;

            if (isConnectingToInternet(getApplicationContext())) {
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();

                try {
                    Log.e("latitude", location.getLatitude() + "");
                    Log.e("longitude", location.getLongitude() + "");

                    jsonObject.put("latitude", location.getLatitude());
                    jsonObject.put("longitude", location.getLongitude());

                    jsonArray.put(jsonObject);

//                    new LocationWebService().execute(new String[] {
//                            "http://estaqueue.udpsa.com/prueba.php", jsonArray.toString() });

                    Log.e("request", jsonArray.toString());

                    //Graba los datos de la posicion en una base de datos interna
                    HashMap<String, String> queryValues = new HashMap<String, String>();
                    queryValues.put("latitude", Double.toString(location.getLatitude()));
                    queryValues.put("longitude", Double.toString(location.getLongitude()));

                    Double Longitude=location.getLongitude();
                    Double Latitude=location.getLatitude();

                    System.out.println("scanneo "+scanneo);
                    System.out.println("fecha "+fecha);
                    System.out.println("hora "+hora);
                    System.out.println("latitude "+Double.toString(location.getLatitude()));
                    System.out.println("longitude "+Double.toString(location.getLongitude()));

                    modelScanneos.insertarScanneo(scanneo,Longitude,Latitude,fecha,hora,0,db);

                   // enviarScanneos envioScanneos= new enviarScanneos();
                    //envioScanneos.execute();

                    enviarScanneos envioScanneos= new enviarScanneos();
                    envioScanneos.execute();




                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

        }


        class enviarScanneos extends AsyncTask<Void, Void, Void> {

            private final ProgressDialog dialog = new ProgressDialog(ServiceLocation.this);
            String estado="";

            @Override
            protected void onPreExecute() {


                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {

                String jsonEnviar="";
                ArrayList<String> listUsario= new ArrayList<String>();
                listUsario=modelUsuarios.getUser(db);
                System.out.println("usuario2 "+usuario);

                send.enviarInformacion(db, listUsario);


                System.out.println("cambiar estado");
                modelScanneos.cambioEstado(db);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                Intent dialogIntent = new Intent(ServiceLocation.this, MenuScanneos.class);
                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(dialogIntent);

                android.os.Process.killProcess(android.os.Process.myPid());
                super.onPostExecute(aVoid);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }
    };

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.v("STOP_SERVICE", "DONE");
        locationManager.removeUpdates(listener);
//        wakeLock.release();

    }

    public static boolean isConnectingToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }
}
