package com.udpsa.controlruta;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.udpsa.controlruta.Connection.DataBase;
import com.udpsa.controlruta.Connection.SendInformation;
import com.udpsa.controlruta.Models.ModelReportes;
import com.udpsa.controlruta.Models.ModelScanneos;
import com.udpsa.controlruta.Models.ModelUsuarios;
import com.udpsa.controlruta.Util.Useful;

import java.util.ArrayList;

public class MenuScanneos extends AppCompatActivity {


    TextView lblUsuario;
    TextView lblNumeroScanneos;
    TextView lblNumeroReportes;
    String usuario;
    String user;
    String numeroScanneos;
    String numeroReportes;

    DataBase Database = new DataBase(this);
    SQLiteDatabase db;
    ModelScanneos modelScanneos;
    SendInformation send;
    Useful use;
    ModelUsuarios modelUsuarios;
    ModelReportes modelReportes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_scanneos);

        send= new SendInformation();
        modelUsuarios= new ModelUsuarios();
        use= new Useful();
        modelReportes= new ModelReportes();

        modelScanneos = new ModelScanneos();
        db = Database.getWritableDatabase();

        lblUsuario = (TextView) findViewById(R.id.lblUsuario);
        lblNumeroScanneos = (TextView) findViewById(R.id.lblNumeroScanneos);
        lblNumeroReportes = (TextView) findViewById(R.id.lblNumeroReportes);

        ArrayList<String> listUser=modelUsuarios.getNombreUser(db);


        usuario=listUser.get(0);
        user=listUser.get(1);

        numeroScanneos=modelScanneos.numeroScanneos(db);

        numeroReportes=modelReportes.numeroScanneos(db);

        lblUsuario.setText(usuario);

        lblNumeroScanneos.setText(lblNumeroScanneos.getText().toString() + ": " + numeroScanneos);

        lblNumeroReportes.setText(lblNumeroReportes.getText().toString() + ": " + numeroReportes);
    }


    public void nuevoScanneo(View view) {

        Intent i = new Intent();
        i.setClass(MenuScanneos.this, Scanner.class);
        startActivity(i);
        android.os.Process.killProcess(android.os.Process.myPid());
    }


    public void transmitir(View view){
        AlertDialog.Builder alert= new AlertDialog.Builder(MenuScanneos.this);
        alert.setMessage("Señor usuario, ¿esta seguro de transmitir información en este momento?");
        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(use.hasInternet(MenuScanneos.this)){
                    enviarScanneosTransmitir envioTransmitir= new enviarScanneosTransmitir();
                    envioTransmitir.execute();
                }else{
                    AlertDialog.Builder ad= new AlertDialog.Builder(MenuScanneos.this);
                    ad.setMessage("Señor usuario, no tiene conexión a internet");
                    ad.setPositiveButton("Aceptar ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });
                    ad.show();
                }

            }
        });
        alert.setNegativeButton("Cancelar ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });

        alert.show();
    }

    public void enviarScanneosCerrarSesion(View view) {

        AlertDialog.Builder alert= new AlertDialog.Builder(MenuScanneos.this);
        alert.setMessage("Señor usuario, ¿esta seguro de cerrar sesión en este momento?");
        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(use.hasInternet(MenuScanneos.this)){
                    enviarScanneosTerminar envioTerminar= new enviarScanneosTerminar();
                    envioTerminar.execute();
                }else{
                    AlertDialog.Builder ad= new AlertDialog.Builder(MenuScanneos.this);
                    ad.setMessage("Señor usuario, no tiene conexión a internet");
                    ad.setPositiveButton("Aceptar ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            return;
                        }
                    });
                    ad.show();
                }

            }
        });
        alert.setNegativeButton("Cancelar ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
           dialog.dismiss();
                return;
            }
        });

        alert.show();

        ArrayList<String> listUsario= new ArrayList<String>();
        listUsario=modelUsuarios.getUser(db);

        String jsonEnviar= modelScanneos.getScanneos(db,listUsario);

        System.out.println("jsonEnviar "+jsonEnviar);
    }

    public void addReport(View view) {
        Intent i= new Intent();
        i.setClass(MenuScanneos.this,Reporte.class);
        startActivity(i);
    }


    class enviarScanneosTransmitir extends AsyncTask<Void, Void, Void> {

        private final ProgressDialog dialog = new ProgressDialog(MenuScanneos.this);


        @Override
        protected void onPreExecute() {

            this.dialog.setCancelable(false);
            this.dialog.setMessage("Enviando Informacion");
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String jsonEnviar="";

            ArrayList<String> listUsario= new ArrayList<String>();
            listUsario=modelUsuarios.getUser(db);

            send.enviarInformacion(db, listUsario);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
           dialog.dismiss();
            super.onPostExecute(aVoid);
        }
    }

    class enviarScanneosTerminar extends AsyncTask<Void, Void, Void> {

        private final ProgressDialog dialog = new ProgressDialog(MenuScanneos.this);


        @Override
        protected void onPreExecute() {

            this.dialog.setCancelable(false);
            this.dialog.setMessage("Enviando Informacion");
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String jsonEnviar="";

            ArrayList<String> listUsario= new ArrayList<String>();
            listUsario=modelUsuarios.getUser(db);


            send.enviarInformacion(db, listUsario);
            use.deleteTables(db);



            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent i = new Intent();
            i.setClass(MenuScanneos.this, MainActivity.class);
            startActivity(i);
            android.os.Process.killProcess(android.os.Process.myPid());
            super.onPostExecute(aVoid);
        }
    }
}
