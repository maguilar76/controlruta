package com.udpsa.controlruta.Models;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by LenovoPc_2 on 16/09/2016.
 */
public class ModelScanneos {

    public void insertarScanneo(String scanneo, Double longitud, Double latitud, String fecha, String hora, int estadoEnvio, SQLiteDatabase db) {

        db.execSQL("INSERT INTO scanneos(scanneo,longitud,latitud,fecha,hora,estadoEnvio) VALUES ('" + scanneo + "','" + longitud + "','" + latitud + "','" + fecha + "','" + hora + "','" + estadoEnvio + "')");
    }

    public String numeroScanneos(SQLiteDatabase db) {
        String numeroScanneos = "";
        Cursor n = db.rawQuery("SELECT COUNT(*) FROM scanneos", null);
        n.moveToFirst();
        numeroScanneos = n.getString(0);
        n.close();

        return numeroScanneos;
    }

    public void cambioEstado(SQLiteDatabase db){
        db.execSQL("UPDATE scanneos SET estadoEnvio='1' ");
    }

    public String getScanneos(SQLiteDatabase db, ArrayList<String> listUsario) {
        JSONArray jsonArray = new JSONArray();


        Cursor c = db.rawQuery("SELECT * FROM scanneos WHERE estadoEnvio='0'", null);

        JSONObject jsonPrincipal = new JSONObject();
        while (c.moveToNext()) {
            JSONObject jsonScanneos = new JSONObject();

            try {
                jsonScanneos.put("usuario", listUsario.get(0));
                jsonScanneos.put("user_id", listUsario.get(1));
                jsonScanneos.put("scanneo", c.getString(1));
                jsonScanneos.put("longitud", c.getString(2));
                jsonScanneos.put("latitud", c.getString(3));
                jsonScanneos.put("fecha", c.getString(4));
                jsonScanneos.put("hora", c.getString(5));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            System.out.println("scanneo" + c.getString(1));
            System.out.println("longitud" + c.getString(2));
            System.out.println("latitud" + c.getString(3));

            jsonArray.put(jsonScanneos);
        }
        c.close();

        try {
            jsonPrincipal.put("scanneos", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonPrincipal.toString();
    }
}
