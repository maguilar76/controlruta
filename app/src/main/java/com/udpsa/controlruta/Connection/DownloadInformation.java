package com.udpsa.controlruta.Connection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LenovoPc_2 on 14/09/2016.
 */
public class DownloadInformation {


    public String validateInformation(String user,String password,String imei){
        System.out.println("llego a validar");
        String response="";

        HttpClient httpClient= new DefaultHttpClient();
        HttpPost post= new HttpPost("https://informes.udpsa.com/svirtual/ControlRuta_Login.php");

        List<NameValuePair> parameters= new ArrayList<NameValuePair>();

        System.out.println("usuario "+user);
        System.out.println("password "+password);


        parameters.add(new BasicNameValuePair("usuario",user));
        parameters.add(new BasicNameValuePair("clave",password));
        parameters.add(new BasicNameValuePair("imei",imei));

        try {
            System.out.println("entro al try");
            post.setEntity(new UrlEncodedFormEntity(parameters));

            System.out.println("executa el cliente");
            HttpResponse resp=httpClient.execute(post);

            System.out.println("obtiene la etnidad");
            HttpEntity ent=resp.getEntity();

            System.out.println("esperando respuesta");
            response= EntityUtils.toString(ent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("retornar ");

        return response;
    }
}
