package com.udpsa.controlruta;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;

import com.udpsa.controlruta.Connection.DataBase;
import com.udpsa.controlruta.Connection.SendInformation;
import com.udpsa.controlruta.Models.ModelReportes;
import com.udpsa.controlruta.Models.ModelUsuarios;
import com.udpsa.controlruta.Util.Useful;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class Reporte extends AppCompatActivity {

    private static final int TOMAR_FOTO = 1;
    Bitmap imagen;
    ModelReportes modelReportes;
    Useful use;

    DataBase Database = new DataBase(this);
    SQLiteDatabase db;

    EditText txtcomentario;

    SendInformation send;
    ModelUsuarios modelUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte);

        txtcomentario = (EditText) findViewById(R.id.txtComentarioReporte);
        modelReportes = new ModelReportes();
        db = Database.getWritableDatabase();
        use = new Useful();

        send = new SendInformation();
        modelUsuarios = new ModelUsuarios();
    }


    public void llamarCamara(View view) {
        Intent camaraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(camaraIntent, TOMAR_FOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TOMAR_FOTO) {

            imagen = (Bitmap) data.getExtras().get("data");
            final ImageView foto = (ImageView) findViewById(R.id.viewMostrarImagen);
            foto.setImageBitmap(imagen);


        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void guardarReporte(View view) {
        String comentario = txtcomentario.getText().toString().trim();
        modelReportes.insertarReporte(db, use.getFecha(), use.getHora(), comentario, imagen);

        //String jsonPrincipal= modelReportes.getReporte(db);

        //System.out.println("jsonPrincipal "+jsonPrincipal);

        if (use.hasInternet(Reporte.this)) {
            enviarReportes envio = new enviarReportes();
            envio.execute();
        } else {
            AlertDialog.Builder ad = new AlertDialog.Builder(Reporte.this);
            ad.setCancelable(false);
            ad.setMessage("Señor usuario, no tiene conexión a internet, el reporte quedará almacenado.");
            ad.setPositiveButton("Aceptar ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent();
                    i.setClass(Reporte.this, MenuScanneos.class);
                    startActivity(i);
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            });

            ad.show();

        }

    }


    class enviarReportes extends AsyncTask<Void, Void, Void> {

        private final ProgressDialog dialog = new ProgressDialog(Reporte.this);
        String estado = "";

        @Override
        protected void onPreExecute() {

            this.dialog.setCancelable(false);
            this.dialog.setMessage("Enviando Informacion");
            this.dialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            String jsonEnviar = "";
            ArrayList<String> listUsario = new ArrayList<String>();
            listUsario = modelUsuarios.getUser(db);


            send.enviarInformacion(db, listUsario);


            System.out.println("cambiar estado");
            modelReportes.cambioEstadoReporte(db);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Intent i = new Intent();
            i.setClass(Reporte.this, MenuScanneos.class);
            startActivity(i);
            android.os.Process.killProcess(android.os.Process.myPid());
            super.onPostExecute(aVoid);
        }
    }


}
