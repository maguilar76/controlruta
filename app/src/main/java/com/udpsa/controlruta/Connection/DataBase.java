package com.udpsa.controlruta.Connection;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by LenovoPc_2 on 14/09/2016.
 */
public class DataBase extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "controlruta.db";

    public DataBase(Context context){
        super(context,DATABASE_NAME,null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE usuarios (_id INTEGER PRIMARY KEY AUTOINCREMENT,user TEXT,password TEXT,nombre TEXT,apellido TEXT,userId TEXT)");
        db.execSQL("CREATE TABLE scanneos (_id INTEGER PRIMARY KEY AUTOINCREMENT,scanneo TEXT,longitud TEXT,latitud TEXT,fecha TEXT,hora TEXT,estadoEnvio INTEGER)");
        db.execSQL("CREATE TABLE Reportes (_id INTEGER PRIMARY KEY AUTOINCREMENT,fecha TEXT,hora TEXT,comentario TEXT,imagen TEXT,estadoEnvio INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String message = "Actualizando base de datos, lo cual borrará toda la información almacenada";
        android.util.Log.w("Usuarios",
                message);


        db.execSQL("DROP TABLE IF EXISTS usuarios");
        db.execSQL("DROP TABLE IF EXISTS scanneos");
        db.execSQL("DROP TABLE IF EXISTS Reportes");


        onCreate(db);
    }
}
